import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AddressService } from '../../../services/Address.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { SubBaseComponent } from '../../Address/sub.base.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditAddressComponent extends SubBaseComponent implements OnInit {

  address: any;
  addressForm: FormGroup;
  title = 'Edit Address';
  
  constructor( http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: AddressService, 
  				private fb: FormBuilder) {
    super(http);
    this.createForm();
   }

  createForm() {
    this.addressForm = this.fb.group({
      street: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required],
      zipCode: ['', Validators.required]
   });
  }
  updateAddress(street, city, state, zipCode) {
    this.route.params.subscribe(params => {
    	this.service.updateAddress(street, city, state, zipCode, params['id'])
      		.then(success => this.router.navigate(['/indexAddress']) );
  });
}

// initialization
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.address = this.service.editAddress(params['id']).subscribe(res => {
        this.address = res;
      });
    });
    
    super.ngOnInit();
  }
}
